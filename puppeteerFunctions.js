import { VIEWPORT_WIDTH, VIEWPORT_HEIGHT } from '../../constants';
import { getRandomArrayValue } from './simple-faker/simple-faker';

// ===XPATH===

export const waitForDisappearXPath = async function (selector, timeout = 7000) {
  try {
    await page.waitForXPath(selector, { hidden: true, timeout });
    return true;
  } catch (e) {
    return false;
  }
};

export const waitForElementXPath = async function (selector, timeout = 7000) {
  return page.waitForXPath(selector, { timeout });
};

export const sendKeysXPath = async function (selector, key) {
  const element = await waitForElementXPath(selector);
  await element.type(key);
};

export const clickElementXPath = async function (selector, attempt = 0) {
  if (attempt > 3) {
    throw new Error(
      `Can not click on element "${selector}" after ${attempt} attempts`,
    );
  }
  const element = await waitForElementXPath(selector, 5000);
  try {
    await element.hover();
    await element.click();
  } catch (e) {
    await clickElementXPath(selector, attempt + 1);
  }
};

const getPropertyXPath = async function (selector, property) {
  await page.waitForXPath(selector, { timeout: 7000 });
  const [node] = await page.$x(selector);
  const {
    _remoteObject: { value },
  } = await node.getProperty(property);
  return value;
};

export const getTextByXPath = async function (selector) {
  return getPropertyXPath(selector, 'textContent');
};

export const getInnerTextByXPath = async function (selector) {
  return getPropertyXPath(selector, 'innerText');
};

export const getAttributeValueXPath = async function (selector) {
  return getPropertyXPath(selector, 'value');
};

export const getPlaceholderValueXPath = async function (selector) {
  return getPropertyXPath(selector, 'placeholder');
};

export const clearAndSendKeysXPath = async function (selectorXpath, key) {
  const element = await waitForElementXPath(selectorXpath);
  await element.click({ clickCount: 3 });
  await page.keyboard.press('Backspace');
  await sendKeysXPath(selectorXpath, key);
};

// ===CSS===

const getPropertyCSS = async function (selectorCSS, property) {
  const element = await page.waitForSelector(selectorCSS, { timeout: 5000 });
  const {
    _remoteObject: { value },
  } = await element.getProperty(property);
  return value;
};

export const getAttributeValueCSS = async function (selectorCSS) {
  return getPropertyCSS(selectorCSS, 'value');
};

export const getTextByCSS = async function (selectorCSS) {
  return getPropertyCSS(selectorCSS, 'innerText');
};

export const getPlaceholderValueCSS = async function (selectorCSS) {
  return getPropertyCSS(selectorCSS, 'innerText');
};

export const waitForDisappearCSS = async function (selectorCSS, timeout = 5000) {
  return page.waitForSelector(selectorCSS, { hidden: true, timeout });
};

export const waitForElementCSS = async function (selectorCSS) {
  return page.waitForSelector(selectorCSS, { timeout: 7000 });
};

export const sendKeysCSS = async function (selectorCSS, key) {
  const element = await waitForElementCSS(selectorCSS);
  await element.type(key);
};

export const clickElementCSS = async function (selectorCSS, attempt = 0) {
  if (attempt > 3) {
    throw new Error(
      `Can not click on element "${selectorCSS}" after ${attempt} attempts`,
    );
  }
  const element = await waitForElementCSS(selectorCSS);
  try {
    await element.click();
  } catch (e) {
    await clickElementCSS(selectorCSS, attempt + 1);
  }
};

export const openInNewTabByCSS = async (selectorCSS) => {
  const element = await waitForElementCSS(selectorCSS);
  await element.click({ button: 'middle' });
};

export const clearInputCSS = async (selectorCSS) => {
  await page.waitForSelector(selectorCSS, { timeout: 7000 });
  await page.click(selectorCSS, { clickCount: 3 });
  await page.keyboard.press('Backspace');
};

export const clearAndSendKeysCSS = async function (selectorCSS, key) {
  await clearInputCSS(selectorCSS);
  await sendKeysCSS(selectorCSS, key);
};

export const clearAndSendKeysCSSWithFocusPage = async function (
  selectorCSS,
  key,
) {
  await page.waitForSelector(selectorCSS, { timeout: 7000 });
  await page.click(selectorCSS, { clickCount: 3 });
  await page.keyboard.press('Backspace');
  await page.focus(selectorCSS);
  await page.type(selectorCSS, key, { delay: 5000 });
};

// ===anotherFunctions===

export const removeHiddenAttribute = async function (selector) {
  await page.$eval(selector, e => e.removeAttribute('hidden'));
};

// element is present-absent custom function
export const elementIsPresentByXpath = async (
  selectorXpath,
  timeout = 5000,
) => {
  try {
    await page.waitForXPath(selectorXpath, { timeout });
    return true;
  } catch (e) {
    console.warn(`Element by Xpath not found: ${selectorXpath}`);
    return false;
  }
};

export const elementIsPresentByCSS = async function (
  selectorCSS,
  timeout = 5000,
) {
  try {
    await page.waitForSelector(selectorCSS, { timeout });
    return true;
  } catch (e) {
    console.warn(`Element by CSS not found: *${selectorCSS}*`);
    return false;
  }
};

// if we expected element should be absent - set low timeout and return appropriate value true
export const elementIsAbsentByXpath = async function (
  selectorXpath,
  timeout = 2000,
) {
  try {
    await page.waitForXPath(selectorXpath, { timeout });
    console.warn(`Element should be absent: *${selectorXpath}*`);
    return false;
  } catch (e) {
    return true;
  }
};

// array of elements by same selector
export const elementAllByXpath = async function (selectorXpath) {
  try {
    await page.waitForXPath(selectorXpath, { timeout: 4000 });
    return page.$x(selectorXpath);
  } catch (e) {
    return 0;
  }
};

export const elementAllByCSS = async function (selectorCSS) {
  try {
    await page.waitForSelector(selectorCSS, { timeout: 4000 });
    return page.$$(selectorCSS);
  } catch (e) {
    return 0;
  }
};

export const elementClickCSSwithNumber = async function (selectorCSS, number) {
  const elements = await elementAllByCSS(selectorCSS);
  if (elements !== 0) {
    await elements[number].click();
    return elements[number];
  }
  console.log(`Error - elements not found: ${selectorCSS}[${number}]`);
  return 0;
};

export const attachFileToElementNumber = async (
  selectorCSS,
  elemNumber,
  filePath,
) => {
  const elemAll = await elementAllByCSS(selectorCSS);
  await elemAll[elemNumber].uploadFile(filePath);
  // Need this hard wait for CI
  await page.waitForTimeout(1000);
};

export const closeNeedlessTab = async () => {
  const tabs = await browser.pages();
  if (tabs.length > 1) {
    await tabs[0].close();
  }
};

export const defineOneActiveTab = async () => {
  await page.waitForTimeout(1000);
  const tabs = await browser.pages();
  await tabs[0].bringToFront();
  global.page = await tabs.shift();
  await Promise.all(tabs.map(async tab => tab.close()));
};

const setViewport = async () => global.page.setViewport({
  width: VIEWPORT_WIDTH,
  height: VIEWPORT_HEIGHT,
});

export const selectTabIndex = async (index, attempt = 0) => {
  if (attempt > 5) throw new Error(`Can not open tab ${index}`);
  const tabs = await browser.pages();
  if (tabs[index] !== undefined) {
    await tabs[index].bringToFront();
    global.page = await tabs[index];
    await setViewport();
  } else {
    await page.waitForTimeout(1500);
    console.log(`Tab index ${index} not open yet`);
    await selectTabIndex(index, attempt + 1);
  }
};

export const openNewTab = async () => {
  global.page = await browser.newPage();
  await setViewport();
};

export const clickOnRandomListElemnt = async (elements) => {
  const getRandomEl = getRandomArrayValue(elements);
  await getRandomEl.click();
};

export const keyboardType = async (text, delay = 100) => page.keyboard.type(text, { delay });
