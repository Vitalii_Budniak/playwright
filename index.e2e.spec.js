const { beforeAll, describe}  = require('@jest/globals');
const playwright = require('playwright');
const { toMatchImageSnapshot } = require('jest-image-snapshot');

describe('Main Page', () => {

    beforeAll(async () => {
        jest.setTimeout(60000);
    });
    expect.extend({ toMatchImageSnapshot });

    it('Check Snapshots', async () => {
        for (const browserType of ['chromium', 'firefox', 'webkit']) {
            const browser = await playwright[browserType].launch({headless: false});
            const context = await browser.newContext();
            const page = await context.newPage();
            await page.goto('http://whatsmyuseragent.org/');
            await page.waitForTimeout(5000);
            await page.screenshot({ path: `example-${browserType}.png` });
            const screen = await page.screenshot({ fullPage: true });
            expect(screen).toMatchImageSnapshot({
                failureThreshold: 5,
                failureThresholdType: 'pixel',
            });
            await browser.close();
        }
    }, 60000);
});

//https://github.com/playwright-community/jest-playwright
